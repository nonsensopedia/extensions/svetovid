<?php

$specialPageAliases = [];

/*
 *  English (English)
 */
$specialPageAliases['en'] = [
	'LinkCreator' => [ 'LinkCreator', 'Link_creator' ]
];

/*
 * Polish
 */
$specialPageAliases['pl'] = [
	'LinkCreator' => [ 'Kreator linków' ]
];